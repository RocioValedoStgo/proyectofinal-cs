import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';

const config: SocketIoConfig = {
  url: environment.wsUrl, options: {}
};

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlumnoComponent } from './components/alumno/alumno.component';
import { ProfesorComponent } from './components/profesor/profesor.component';
import { AsignaturaComponent } from './components/asignatura/asignatura.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';


//Para que angular se prepare para http
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { environment } from 'src/environments/environment.prod';
import { DatosAlumnoComponent } from './components/datos-alumno/datos-alumno.component';
import { DatosProfesorComponent } from './components/datos-profesor/datos-profesor.component';
import { DatosAsignaturaComponent } from './components/datos-asignatura/datos-asignatura.component';
import { EditarComponent } from './components/editar/editar.component';

@NgModule({
  declarations: [
    AppComponent,
    AlumnoComponent,
    ProfesorComponent,
    AsignaturaComponent,
    HomeComponent,
    FooterComponent,
    DatosAlumnoComponent,
    DatosProfesorComponent,
    DatosAsignaturaComponent,
    EditarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SocketIoModule.forRoot(config)

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
