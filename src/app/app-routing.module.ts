import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AlumnoComponent } from './components/alumno/alumno.component';
import { ProfesorComponent } from './components/profesor/profesor.component';
import { AsignaturaComponent } from './components/asignatura/asignatura.component';
import { HomeComponent } from './components/home/home.component';
import { DatosAlumnoComponent } from './components/datos-alumno/datos-alumno.component';
import { DatosProfesorComponent } from './components/datos-profesor/datos-profesor.component';
import { DatosAsignaturaComponent } from './components/datos-asignatura/datos-asignatura.component';
import { EditarComponent } from './components/editar/editar.component';

const routes: Routes = [
  {
    path:'',
    component: HomeComponent
  },
  {
    path:'alumno',
    component: AlumnoComponent
  },
  {
    path: 'profesor',
    component: ProfesorComponent
  },
  {
    path: 'asignatura',
    component: AsignaturaComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'alumno-datos',
    component: DatosAlumnoComponent
  },
  {
    path: 'profesor-datos',
    component: DatosProfesorComponent
  },
  {
    path: 'asignatura-datos',
    component: DatosAsignaturaComponent
  },
  {
    path: 'editar',
    component: EditarComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
