import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosAsignaturaComponent } from './datos-asignatura.component';

describe('DatosAsignaturaComponent', () => {
  let component: DatosAsignaturaComponent;
  let fixture: ComponentFixture<DatosAsignaturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosAsignaturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosAsignaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
