import { Component, OnInit } from '@angular/core';
import { AsignaturaService } from 'src/app/services/asignatura.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-asignatura',
  templateUrl: './asignatura.component.html',
  styleUrls: ['./asignatura.component.css']
})
export class AsignaturaComponent implements OnInit {

  formLogin: FormGroup;
  formDatos: FormGroup;

  constructor(public loginService: AsignaturaService, private formBuilder: FormBuilder) {
    this.formLogin = this.formBuilder.group({
      'username': ['valedo98'],
      'password' : ['SuperJunior']
    });

    this.formDatos = this.formBuilder.group({ 
      nombre: new FormControl(''),
      materia: new FormControl(''),
      horario: new FormControl(''),
    });
   }

  ngOnInit() {
    console.log(this.formLogin.value);
    this.loginService.login(this.formLogin.value).subscribe(
      response => {
        localStorage.setItem('token', response.token); 
        console.log(response);
      }
    );
  }

  post() {
    //METODO POST
    console.log(this.formDatos.value);
    this.loginService.post(this.formDatos.value).subscribe(
      response => {
        console.log(response);
      }
    );
  }

}
