import { Component, OnInit } from '@angular/core';
import { ProfesorService } from 'src/app/services/profesor.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html',
  styleUrls: ['./profesor.component.css']
})
export class ProfesorComponent implements OnInit {
  formLogin: FormGroup;
  formDatos: FormGroup;

  constructor(public loginService: ProfesorService, private formBuilder: FormBuilder) {
    this.formLogin = this.formBuilder.group({
      'username': ['valedo98'],
      'password' : ['SuperJunior']
    });

    this.formDatos = this.formBuilder.group({ 
      nombre: new FormControl(''),
      apellidoP: new FormControl(''),
      apellidoM: new FormControl(''),
      edad: new FormControl(''),
      direccion: new FormControl(''),
      email: new FormControl(''),
      genero: new FormControl(''),
    });
   }

  ngOnInit() {
    console.log(this.formLogin.value);
    this.loginService.login(this.formLogin.value).subscribe(
      response => {
        localStorage.setItem('token', response.token); 
        console.log(response);
      }
    );
  }

  post() {
    //METODO POST
    console.log(this.formDatos.value);
    this.loginService.post(this.formDatos.value).subscribe(
      response => {
        console.log(response);
      }
    );
  }
}
