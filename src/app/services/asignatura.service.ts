import { Injectable } from '@angular/core';
import { WebsocketService } from './websocket.service';

import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { API } from '../app-config';

const httpOptions = {
  headers : new HttpHeaders({
    'Content-Type' : 'application/json',
    'Authorization' : 'Token ' + localStorage.getItem('token')
  })
}

@Injectable({
  providedIn: 'root'
})

export class AsignaturaService {
  api: string = API;

  constructor(
    public wsService: WebsocketService, 
    private http: HttpClient
  ) { }

  login(params: string): Observable<any> {
    const httpOptions2 = {
      headers : new HttpHeaders({
        'Content-Type' : 'application/json',
      })
    }
    return this.http.post(`${this.api}login/`, params, httpOptions2);
  }

  post(params: string): Observable<any> {
    return this.http.post(`${this.api}user/lista/`, params, httpOptions);
  }

  // get(params: string): Observable<any> {
    //   return this.http.get(`${this.api}user/lista/`, httpOptions);
    // }
}
